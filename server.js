const cors = require('cors');
const fs = require('fs');
const express = require('express');
const path = require('path');
const { deepStrictEqual } = require('assert');
const app = express();
const PORT = 8080;

//Path to files folder
const dirPath = path.join(__dirname, 'files/');
//Regex to check supported file extensions
const extRegex =
  /([a-zA-Z0-9\s_\\.\-\(\):])+(.log|.txt|.json|.yaml|.xml|.js)$/i;

if (!fs.existsSync(dirPath)) {
  fs.mkdirSync(dirPath);
}

app.use(cors());
app.use(express.json());

//Create file
app.post('/api/files', (req, res) => {
  const filename = req.body.filename;
  const content = req.body.content;

  try {
    if (!filename) {
      console.log("ERROR 400: Please specify 'filename' parameter");
      res.status(400).send({
        message: "Please specify 'filename' parameter",
      });
      return;
    } else if (!content) {
      console.log("ERROR 400: Please specify 'content' parameter");
      res.status(400).send({
        message: "Please specify 'content' parameter",
      });
      return;
    } else if (!extRegex.test(filename)) {
      console.log("ERROR 400: Unaccaptable 'filename' extension");
      res.status(400).send({
        message: "Unaccaptable 'filename' extension",
      });
      return;
    } else if (fs.existsSync(dirPath + filename)) {
      console.log("ERROR 400: File with this 'filename' already exists");
      res.status(400).send({
        message: "File with this 'filename' already exists",
      });
      return;
    }

    fs.appendFile(dirPath + filename, content, () => {
      console.log(`File '${filename}' created successfully.`);
      res.send({ message: 'File created successfully.' });
    });
  } catch (err) {
    console.log('ERROR 500: ' + err);
    res.status(500).send({
      message: 'Server error',
    });
  }
});

//Get all files
app.get('/api/files', (req, res) => {
  try {
    fs.readdir(dirPath, (err, files) => {
      res.send({
        message: 'Success',
        files,
      });
    });
  } catch (err) {
    console.log('ERROR 500: ' + err);
    res.status(500).send({
      message: 'Server error',
    });
  }
});

//Get file
app.get('/api/files/:id', (req, res) => {
  const filename = req.params.id;

  try {
    if (!fs.existsSync(dirPath + filename)) {
      console.log("ERROR 400: File with this 'filename' does not exists");
      res.status(400).send({
        message: "File with this 'filename' does not exists",
      });
      return;
    }

    const extention = path.extname(filename).substring(1);
    const content = fs.readFileSync(dirPath + filename, 'utf8');
    const uploadedDate = fs.statSync(dirPath + filename).birthtime;

    res.send({
      message: 'Success',
      filename,
      content,
      extention,
      uploadedDate,
    });
  } catch (err) {
    console.log('ERROR 500: ' + err);
    res.status(500).send({
      message: 'Server error',
    });
  }
});

//Edit file
app.put('/api/files/:id', (req, res) => {
  const filename = req.params.id;
  const newFilename = req.body.filename;
  const newContent = req.body.content;

  try {
    if (!newFilename) {
      console.log("ERROR 400: Please specify 'filename' parameter");
      res.status(400).send({
        message: "Please specify 'filename' parameter",
      });
      return;
    } else if (!newContent) {
      console.log("ERROR 400: Please specify 'content' parameter");
      res.status(400).send({
        message: "Please specify 'content' parameter",
      });
      return;
    } else if (!extRegex.test(newFilename)) {
      console.log("ERROR 400: Unaccaptable 'filename' extension");
      res.status(400).send({
        message: "Unaccaptable 'filename' extension",
      });
      return;
    } else if (!fs.existsSync(dirPath + filename)) {
      console.log("ERROR 400: File with this 'filename' does not exists");
      res.status(400).send({
        message: "File with this 'filename' does not exists",
      });
      return;
    } else if (
      fs.existsSync(dirPath + newFilename) &&
      newFilename !== filename
    ) {
      console.log("ERROR 400: File with this 'filename' already exists");
      res.status(400).send({
        message: "File with this 'filename' already exists",
      });
      return;
    }

    const content = fs.readFileSync(dirPath + filename).toString();

    if (newFilename !== filename) {
      fs.rename(dirPath + filename, dirPath + newFilename, () => {});
    }
    if (newContent !== content) {
      fs.writeFileSync(dirPath + filename, newContent, 'utf-8');
    }

    console.log('File edited successfully');
    res.send({
      message: 'File edited successfully',
    });
  } catch (err) {
    console.log('ERROR 500: ' + err);
    res.status(500).send({
      message: 'Server error',
    });
  }
});

app.listen(PORT, () => console.log(`Server listening on port: ${PORT}`));
